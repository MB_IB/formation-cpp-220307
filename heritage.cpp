#include <iostream>
#include <memory>
#include <vector>

#include "heritage.h"

using namespace std;

// m�re \ heritage public     protected   private
// public          public     protected   private
// protected       protected  protected   private
// private            /           /         /
class Ouvrage {
public:
    string nom;
    Ouvrage(string n):nom(n) {}
    virtual void afficher() { cout << nom << ". "; }
    virtual ~Ouvrage() {}
};

class Livre : virtual public Ouvrage {
public:
    string ecrivain;
    Livre(string n, string e):Ouvrage(n),ecrivain(e) {}
    void afficher() override { Ouvrage::afficher(); cout << "De " << ecrivain << ". "; }
};

class Cd : virtual public Ouvrage {
public:
    int duree;
    Cd(string n, int d):Ouvrage(n), duree(d) {}
    void afficher() override { Ouvrage::afficher(); cout << duree << "min. "; }
};

class LivreAudio : public Livre, public Cd {
public:
    string langue;
    LivreAudio(string n, string e, int d, string l):Ouvrage(n), Livre(n,e), Cd(n,d), langue(l) {}
    void afficher() override { Livre::afficher(); Cd::afficher(); cout << langue << ". "; }
};

void heritage() noexcept
{
    {
        cout << "** Heritage" << endl;
        Ouvrage o1 { "Asterix le gaulois" };
        o1.afficher();
        cout << endl;
        Livre l1 { "Odyssee", "Homere" };
        l1.afficher();

        vector<unique_ptr<Ouvrage>> v;
        v.push_back(make_unique<Ouvrage>("Martine � la plage"));
        v.push_back(make_unique<Livre>("Illiade", "Homere"));
        v.push_back(make_unique<Livre>("Le rouge et le noir", "Stendhal"));
        cout << endl << "* Tous : " << endl;
        for(unique_ptr<Ouvrage>& o : v) {
            o->afficher();
            cout << endl;
        }
    }
    LivreAudio la1("Le petit Nicolas", "Goscinny", 58, "fr");
    cout << endl;
    la1.afficher();
    cout << endl;
    cout << "Tailles : " << sizeof(Ouvrage) << ", "  << sizeof(Livre) << ", "
        << sizeof(Cd) << ", " << sizeof(LivreAudio) << ". ";
    // h�ritage en diamant normal : 40, 72, 48, 152
    // h�ritage virtuel : 40, 80, 56, 128
}
