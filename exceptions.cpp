#include <iostream>

#include "exceptions.h"

using namespace std;

void verification(int age) {
    if(age < 18)
        throw range_error("Trop jeune");
}

int maxAnneesDePermis(int age) noexcept {
    const int MIN_AGE = 18;
    if(noexcept(verification(age)))
        verification(age); // jamais ex�cut�
    int res = age - MIN_AGE;
    return res;
}

void exceptions() noexcept {
    cout << "** Exceptions"<< endl;
    int age;
    cin >> age;
    try {
        if(age == 0)
            throw 0;
        if(age < 0)
            throw range_error("Age trop petit");
        int vie_par_ans = 1/age; // ne provoquerait pas une exception
        cout << "Age : " << age << ", vies par ans : " << vie_par_ans << endl;
        int maxAnnees = maxAnneesDePermis(age);
        cout << "Max ann�es de permis : " << maxAnnees << endl;

    } catch(int& exc) {
        if(exc == 0)
            cout << "Age doit �tre diff�rent de 0" << endl;
        else
            cout << "Exception num�rique" << endl;
    } catch(range_error& exc) {
        cout << "Exception d'�tendue : " << exc.what() << endl;
    } catch(...) {
        cout << "Exception ! " << endl;
    }
}
