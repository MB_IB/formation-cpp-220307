#include <iostream>

#include "move.h"

using namespace std;

void valeursGaucheDroite() {
    int x = 18; // x : lvalue, 18 : rvalue
    int y = x; // x, y : lvalues
    int z = x + y; // x, y, z : lvalue ; x + y : rvalue
    struct S1 {
        int f1() { return 3; }
        int& f2(int& n) { return n; }
    };
    S1 s1;
    int r1 = s1.f1(); // r1 : lvalue ; s1.f1() : rvalue
    int r2 = s1.f2(x); // r2, s1.f2(x) : lvalue
    s1.f2(x) = 11; //ok
    cout << x << endl; // 11
}

void variableTemporaires() {
    int x = 18;
    int& y = x; // lvalue reference

    struct S1 {
        void f1(int n) { n++; }
        void f2(int& n) { n++; }
        int f3() { return 13; }
        int f4(int n) { return n+3; }
    };
    S1 s1;
    s1.f1(x);
    cout << x << endl; // 18
    s1.f2(x);
    cout << x << endl; // 19
    // interdit : s1.f2(8);

    int v1 = s1.f3();
    // interdit, r�f�rence vers rvalue : int& v2 = s1.f3();
    const int& v3 = s1.f3();
    const int& v4 = s1.f4(x); //v4 => 22
    int&& v5 = s1.f3();
    v5 += 100;
    cout << "v5 : " << v5 << endl; // 113

    int&& v6 = s1.f3() * 2;
    v6 += s1.f3() + 5;
    cout << "v6 : " << v6 << endl; // 44

    struct S2 {
        void f1(int& n) { cout << "int& " << n << endl; }
        void f1(int&& n) { cout << "int&& " << n << endl; }
    };
    S2 s2;
    s2.f1(x); // int& 19
    s2.f1(3); // int&& 3
    s2.f1(2+3); // int&& 5
    s2.f1(x*2); // int&& 6
    s2.f1(s1.f3()); // int&& 13
    s2.f1(v5); // int& 113
    s2.f1(v5+0); // int&& 113
    s2.f1(std::move(v5)); // int&& 113
}


void deplacements() {
    class TableauEntiers {
    public:
        TableauEntiers(): taille(0), valeurs(new int[0]) { cout << "ctor()" << endl; }
        TableauEntiers(unsigned t): taille(t), valeurs(new int[t]) { cout << "ctor(t)" << endl; }
        TableauEntiers(const TableauEntiers& src):
                taille(src.taille), valeurs(new int[src.taille]) {
            cout << "ctor(TE&)" << endl;
            for(unsigned i = 0; i<taille ; i++)
                valeurs[i] = src.valeurs[i];
        }
        TableauEntiers(TableauEntiers&& src):
                taille(src.taille), valeurs(src.valeurs) {
            cout << "ctor(TE&&)" << endl;
            src.valeurs = nullptr;
        }
        TableauEntiers& operator=(const TableauEntiers& src) {
            cout << "=&" << endl;
            if(&src != this) {
                if(valeurs != nullptr)
                    delete [] valeurs;
                taille = src.taille;
                valeurs = new int[src.taille];
                for(unsigned i = 0; i<taille ; i++)
                    valeurs[i] = src.valeurs[i];
            }
            return *this;
        }
        TableauEntiers& operator=(TableauEntiers&& src) {
            cout << "=&&" << endl;
            if(&src != this) {
                taille = src.taille;
                valeurs = src.valeurs;
                src.valeurs = nullptr;
            }
            return *this;
        }
        virtual ~TableauEntiers() { cout << "~()" << endl; delete [] valeurs; }
        int * getValeurs() { return valeurs; }
    private:
        unsigned taille;        int * valeurs;
    };

    TableauEntiers t1 { 3 }; // tableau de 3 entiers
    int * vs = t1.getValeurs();
    vs[0] = 2; vs[1] = 8; vs[2] = 1;
    cout << vs[2] << endl; // 1
    // interdit, taille inconnue : for(int x:t1.getValeurs()) cout << x;
    TableauEntiers t2 { t1 }; // appelle le constructeur par recopie
    TableauEntiers t3; // appelle le ctor ()
    t3 = t1; // appelle l'op�rateur =

    cout << "***** D�placement : " << endl;
    struct S1 { TableauEntiers getTab(){ return TableauEntiers { 10000 }; } };
    S1 s1;
    TableauEntiers t4;
    t4 = s1.getTab(); // =&&
    TableauEntiers t5 { s1.getTab() }; // ctor(TE&&)
}

void deplacementsDeThis() {
    struct S1 {
        int x;
        S1(int x): x(x) {}
        void showXXXX() const & { int res = x*x*x*x; cout << res << " (version lvalue)" << endl; }
        void showXXXX() && { x = x*x*x*x; cout << x << " (version rvalue)" << endl; }
    };
    S1 s1 { 3 }; //81
    s1.showXXXX();
    S1{ 4 }.showXXXX(); //256
}

void move() {
    cout << "Move semantics : " << endl;
    // valeursGaucheDroite();
    // variableTemporaires();
    // deplacements();
    deplacementsDeThis();
}
