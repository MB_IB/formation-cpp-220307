#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;

void threads() noexcept {
    cout << "** Threads" << endl;
    int cnt = 0;
    mutex mutex_cnt;
    thread t2 { [&](){
        cout << "Thread secondaire : " << this_thread::get_id() << endl;

        for(unsigned i = 0;i<1000 ; i++) {
            this_thread::sleep_for(1ms);
            mutex_cnt.lock();
            cnt++;
            mutex_cnt.unlock();
        }

    } };

    cout << "Thread principal : " << this_thread::get_id() << endl;

    for(unsigned i = 0;i<1000 ; i++) {
        this_thread::sleep_for(1ms);
        mutex_cnt.lock();
        cnt++;
        mutex_cnt.unlock();
    }

    t2.join();

    cout << "Fin : " << cnt << endl;    // toujours 2000
}
