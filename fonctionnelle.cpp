#include <iostream>
#include <functional>
#include <vector>

using namespace std;

int calculEnfant(int mois) { return mois * 3 - 2; }
int calculAdo(int mois) { return 2 + mois * 2; }
int calculAdulte(int mois) { return mois * 2; }

void variableFonctions() noexcept {
    cout << "Adulte/ado/enfant : 0/1/2 ?";
    int choix;
    cin >> choix;
    function<int(int)> fcalcul;
    switch(choix) {
    case 0: fcalcul = calculAdulte; break;
    case 1: fcalcul = calculAdo; break;
    case 2: fcalcul = calculEnfant; break;
    }
    cout << "En 6 mois : " << fcalcul(6) << endl;

    struct S1 {
        int mois;
        void afficheLecture() { cout << mois * 3 << " ouvrages" << endl; }
        void afficheLecture(int livreParMois) {
            cout << mois * livreParMois << " ouvrages" << endl;
        }
    };
    function<void(S1)> faffiche = mem_fn<void()>(&S1::afficheLecture);
    S1 s1;
    s1.mois = 4;
    faffiche(s1);
    //ou
    faffiche(S1 { 4 });

    function<void(S1,int)> faffiche2 = mem_fn<void(int)>(&S1::afficheLecture);
    faffiche2(s1, 2);
}

int livreGroupe(int enfant, int adulte, int temps) {
    return (enfant*2 + adulte ) * (temps - 1);
}

void curryfication() noexcept {
    function<int(int, int, int)> fcomplete = livreGroupe;
    cout << fcomplete(2, 1, 3) << endl;

    function<int(int,int)> ft = bind(livreGroupe, placeholders::_1, placeholders::_2, 12);
    cout << ft(6, 0) << endl; //132
    function<int(int,int)> ft3ans = bind(livreGroupe, placeholders::_1, placeholders::_2, 36);
    cout << ft3ans(6, 0) << endl; //420
    function<int(int)> fea = bind(livreGroupe, 5, 1, placeholders::_1);
    cout << fea(3) << endl; // 88
    function<int(int, int)> fe = bind(livreGroupe, 0, placeholders::_2 , placeholders::_1);
    cout << fe(12, 3) << endl; // 3 adultes, 12 mois, 0 enfants

    function<int()> f = bind(livreGroupe, 3, 2, 12);
    cout << f() << endl; //88
}

void ajouter(unsigned& x) { x++; }

void adaptationDesReferences() {
    string ok {"ok"};
    vector<string> v1 { ok, ok, ok };
    for(string s:v1)
        s += "!";
    cout << ok << ", " << v1[0] << endl; // ok, ok

    for(string& s:v1)
        s += "!";
    cout << ok << ", " << v1[0] << endl; // ok, ok!

    // vector<string&> v2 { ok, ok, ok };
    vector<reference_wrapper<string>> v2 { ref(ok), ref(ok), ref(ok) };
    for(string& s:v2)
        s += "!";
    cout << ok << ", " << v2[0].get() << endl; // ok!!!, ok!!!

    unsigned a = 5;
    auto fAjoutA = bind(ajouter, ref(a));
    fAjoutA();
    fAjoutA();
    cout << a << endl; // 7

    const int cte = 2390;
    reference_wrapper<const int> rw = cref(cte);
    // interdit : ref(2);, cref(2);
}

auto getFct() {
    int n = 3;
    return [&n](){ cout << n; };
}

void lambdas() noexcept {
    function<double(double)> mulBy3 = [](double x)->double{ return x*3; };
    cout << "5*3 : " << mulBy3(5) << endl;

    function<void()> ditBonjour = []{ cout << "Bonjour !" << endl; };
    ditBonjour();

    int i = 23;
    // non : auto afficheI = []{ cout << i << endl; };
    auto afficheI = [i]{ cout << i << endl; };
    afficheI(); // 23
    i = 45;
    afficheI(); // 23

    auto afficheIActuel = [&i]{ cout << i << endl; };
    i = 67;
    afficheIActuel();

    auto fClosure = getFct();
    fClosure(); // 3

    auto afficheIActuelx3 = [=,&i]{ cout << mulBy3(i) << endl; };
    afficheIActuelx3();
}

void fonctionnelle() noexcept {
    cout << "* Prog fonctionnelle" << endl;
    // variableFonctions();
    // curryfication();
    // adaptationDesReferences();
    lambdas();
}
