#include <iostream>

#include "nouveautes.h"
#include "move.h"
#include "ressources.h"
#include "exceptions.h"
#include "heritage.h"
#include "fonctionnelle.h"
#include "templates.h"
#include "bibliotheques.h"
#include "threads.h"

using namespace std;

int main()
{
    cout << "Bienvenue dans notre médiatheque !" << endl;
    // nouveautes();
    // move();
    // ressources();
    // exceptions();
    // heritage();
    // fonctionnelle();
    // templates();
    // bibliotheques();
    threads();
    return 0;
}
