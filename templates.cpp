#include <iostream>
#include <functional>

using namespace std;

template<typename T> T mulPlusAdd(T x, T  y) { return x*y+x+y; }
template<> char mulPlusAdd(char x, char y) { return x+y; } // spécialisation complete
// non correspondant : template<> char mulPlusAdd(char x, int y) { return x+y; }
template<> long mulPlusAdd(long x, long y) = delete;

// autorisé !? : char mulPlusAdd(char x, char y) { return x+y; }

template<typename T> const T PI = T(3.141579);

void baseDesTemplates() noexcept {
    int a = 2, b = 5;
    cout << mulPlusAdd(a, b) << endl;
    float c = 3.4f, d = 2.4f;
    cout << mulPlusAdd<float>(c, d) << endl;
    cout << mulPlusAdd<double>(c, d) << endl; // ok mais plus lent
    // a et c de type différents : cout << mulPlusAdd(a, c) << endl;
    // " " pas multipliable : cout << mulPlusAdd(" ", " ") << endl;

    // specialise :
    cout << mulPlusAdd(char(2), (char)3) << endl;

    cout << "pi : " << PI<float> << endl;
}



template<typename T1, typename T2> T1 mulPlusAdd2(T1 x, T2 y) { return x*y+x+y; }
template<typename T1> T1 mulPlusAdd2(T1 x, double y) { return x*y+x+y+32; }

template<typename T1, typename T2>
        decltype(auto) mulPlusAdd3(T1 x, T2 y) { return x*y+x+y; }

template<typename T1> struct Wrapper {
    T1 valeur;
    Wrapper(T1 v): valeur(v) {}
    template<typename T2> T2 getMulBy(T2 x) { return valeur * x; }
};

template<typename T1, typename T2> struct Wrapper2 { T1 x; T2 y; };
template<typename T1> struct Wrapper2<T1, float> { T1 x; float y; };
template<> struct Wrapper2<float, float> { float x; float y; };

void genericiteMultiple() noexcept {
    cout << mulPlusAdd2(4, 2.3f) << endl; // 15
    Wrapper<int> w1 { 5 };
    Wrapper<string> w2 { "ok" };
    cout << w1.getMulBy(4.2) << endl; // entier * double => double

    // pas de secialisation progressive sur les fonction
    cout << mulPlusAdd2(1, 1.0) << endl; // 35
    cout << mulPlusAdd2<int,double>(1, 1.0) << endl; // 3 ?!

    // cout << mulPlusAdd3(4, 2.3f) << endl; // 15.5

    Wrapper2<double, float> w3; // utilise la deuxieme version
}

template<int8_t V, typename T> T divBy(T x) { return x/V; }

void templateParEntier() noexcept {
    cout << divBy<4, int>(24) << endl; // 6
    // #include <functional>
    function<int(int)> f5 = divBy<5, int>;
}

template<typename T> struct A { T x; };
struct B1:A<int> { };
template<typename T> struct B2 : A<T> { };
template<typename T1, typename T2> struct B3 : A<T1> { T2 y; };
template<typename T> struct B4 : A<int> { T y; };

void templateEtHeritage() noexcept {
    B3<float, double> b3;
}

// version pour unsigned (exemple : unsigned long)
template<typename T, typename std::enable_if_t<is_unsigned<T>::value>* =nullptr >
  T valeurSiNonSigne(T x) { return x; }

// version pour signed (exemple : float)
template<typename T, typename std::enable_if_t<!is_unsigned<T>::value>* =nullptr >
  T valeurSiNonSigne(T x) { return 0; }

void traits() noexcept {
    if(is_unsigned<int>::value)
        cout << "int est unsigned" << endl;
    else
        cout << "int n'est pas unsigned" << endl;
    cout << valeurSiNonSigne(3.4f) << "," <<
        valeurSiNonSigne(unsigned(54)) << endl; // 0,54
}

void templates() noexcept {
    cout << "** Templates " << endl;
    // baseDesTemplates();
    // genericiteMultiple();
    // templateParEntier();
    // templateEtHeritage();
    traits();
}
