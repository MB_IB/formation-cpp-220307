#include <iostream>
#include <memory>

#include "ressources.h"

using namespace std;

void pointeursUniques() {
    struct Usager {
        string nom;
        int age;
        Usager(string n, int a):nom(n), age(a) {}
        void afficher() { cout << nom << ", " << age << " ans" << endl; }
        virtual ~Usager() { cout << "~U" << endl; }
    };

    // Usager * u1 = new Usager("Bob", 8);
    // u1->afficher();
    // delete u1;
    unique_ptr<Usager> u1 (new Usager("Bob", 8));
    u1->afficher();

    // ctor(&u) deleted : unique_ptr<Usager> u2 = u1;
    // operator=() deleted : unique_ptr<Usager> u3; u3 = u1;

    struct S1 {
        void montrer(unique_ptr<Usager> u) { u->afficher(); }
        void montrer2(unique_ptr<Usager>* u) { (*u)->afficher(); }
        void montrer3(unique_ptr<Usager>& u) { u->afficher(); }
        void montrer4(Usager* u) { u->afficher(); delete u; }
        void montrer5(unique_ptr<Usager>&& u) { u->afficher(); }
    };
    S1 s1;
    // ctor(&u) deleted : s1.montrer(u1);
    s1.montrer2(&u1);
    s1.montrer3(u1);
    s1.montrer4(u1.release());
    // interdit : u1->afficher();
    s1.montrer5(unique_ptr<Usager> (new Usager("Ann",32)));
    unique_ptr<Usager> u3 (new Usager("carl", 18));
    s1.montrer5(std::move(u3));
    // interdit : u3->afficher();
    unique_ptr<Usager> u4 = make_unique<Usager>("Dana",89);
    u4->afficher();

    struct S2 {
        double somme(unique_ptr<double[]>& t, unsigned taille) {
            double v = 0;
            for(unsigned i = 0 ; i < taille ; i++)
                v += t[i];
            return v;
        }
    };
    S2 s2;
    unique_ptr<double[]> t1 (new double[3]);
    unique_ptr<double[]> t2 = make_unique<double[]> (4);
    t1[0] = 18.343;
    t1[1] = 1;
    t1[2] = 1;
    cout << t1[0] << endl;
    cout << "Somme de t1 : " << s2.somme(t1, 3) << endl;
}

void pointeursPartages() {
    shared_ptr<string> p1 (new string("Hello"));
    shared_ptr<string> p2 = make_shared<string>("Priviet");
    cout << *p1 << "," << *p2 << endl;

    cout << "(" << p1.use_count() << "," << p2.use_count() << ")" << endl; // 1,1
    shared_ptr<string> p3 = p2;
    cout << "(" << p1.use_count() << "," << p2.use_count() <<
        "," << p3.use_count() << ")" << endl; // 1,2,2
    cout << "p3 : " << *p3 << endl;

    p2.reset();
    cout << "(" << p1.use_count() << "," << p2.use_count() <<
        "," << p3.use_count() << ")" << endl; // 1,0,1
    if(!p2)
        cout << "p2 est vide" << endl;
}

void pointeursFaibles() {
    struct Chat {
        string nom;
        weak_ptr<Chat> ennemi;
        Chat(string n):nom(n), ennemi() {}
        virtual ~Chat(){ cout << "~Chat" << endl; }
    };
    shared_ptr<Chat> tom = make_shared<Chat>("Tom");
    shared_ptr<Chat> pinky = make_shared<Chat>("Pinky");
    shared_ptr<Chat> socrate = make_shared<Chat>("Socrate");
    tom->ennemi = pinky;
    pinky->ennemi = socrate;
    socrate->ennemi = tom;

    if(! tom->ennemi.expired()) {
        cout << "Ennemi de Tom : " << tom->ennemi.lock()->nom << endl;
    }

    // en cas de paralellisme :
    shared_ptr<Chat> ennemiDeTom = tom->ennemi.lock();
    cout << "Ennemi de Tom : " << ennemiDeTom->nom << endl;
    ennemiDeTom.reset();
}

void ressources() {
    cout << "** Ressources" << endl;
    // pointeursUniques();
    // pointeursPartages();
    pointeursFaibles();
}
