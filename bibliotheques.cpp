#include <iostream>
#include <chrono>
#include <unordered_map>
#include <vector>
#include <array>
#include <algorithm>

#include "bibliotheques.h"

using namespace std;

void conversions() {
    double d = 3.424;
    string s = to_string(d);
    d = stod(s);
    cout << d << endl;
}

void chronos() {
    using namespace std::chrono;
    time_point<system_clock> now = system_clock::now();
    time_point<system_clock> demain = now + 24h;
    time_point<system_clock> demainSoir = demain + 180min;
    duration<double, std::milli> attente = demainSoir - now;

    time_t demainSoirTT = system_clock::to_time_t(demainSoir);
    tm demainSoirTM = *localtime(&demainSoirTT);
    cout << demainSoirTM.tm_hour << ":" << demainSoirTM.tm_min << endl;
}

void collections() {
    // vector / list / forward_list
    // set / map / ... / unordered_multimap
    // #include <unordered_map>
    unordered_multimap<string, int> usagers = {
        {"Bob",8}, {"Ann",23}, {"Bob", 76} };
    if(usagers.find("Ann") != usagers.end())
        cout << usagers.find("Ann")->first << endl; // Ann
    cout << usagers.find("Bob")->second << endl; // 76

    vector<int> v1 { 3, 5, 3, 2} ;
    for(vector<int>::const_iterator it = v1.cbegin(); it!=v1.cend() ; it++)
        cout << *it << endl;

    array<double, 2> a { 4.5, 9.3} ;
    cout << a[1] << endl;

    auto t1 = make_tuple("Emile Zola", "Germinal", 1904);
    cout << get<0>(t1) << endl; // Emile Zola

    // #include <algorithm>
    // 0 0 1
    cout << all_of(v1.cbegin(), v1.cend(), [](double d)->bool{return d>5;}) << endl;
    cout << any_of(v1.cbegin(), v1.cend(), [](double d)->bool{return d>5;}) << endl;
    cout << none_of(v1.cbegin(), v1.cend(), [](double d)->bool{return d>5;}) << endl;

}

void bibliotheques() noexcept {
    cout << "** Bibliotheques" << endl;
    // conversions();
    // chronos();
    collections();
}
