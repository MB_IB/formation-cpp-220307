#include <iostream>
#include <vector>
#include <map>
#define NDEBUG
#include <cassert>

#include "nouveautes.h"

using namespace std;

void nullPtr() {
    bool* a = NULL; // ne plus utiliser
    bool* b = nullptr; // type : nullptr_t

    long long ia = (long long)(a); // ok
    long long ib = (long long)(b); // ok
    // interdit : long long in = nullptr;
    long long iN = NULL; // ok
    cout << "ib : " << ib << endl; // 0
    if(b == nullptr) {
        cout << "b est nul" << endl;
    }
}

void types() {
    // Linux 64bits - 4/8/8
    // Windows 64bits - 4/4/8
    int x; // >= 2o
    cout << "int : " << sizeof(x) << "octets" << endl;
    long y; // >= 4o
    cout << "long : " << sizeof(y) << "octets" << endl;
    long long z; // >= 8o
    cout << "long long : " << sizeof(z) << "octets" << endl;
    // char* p; // ??? suivant x bits
    // cout << "pointeur : " << sizeof(p) << "octets" << endl;

    int32_t a = INT32_MIN; // 4 octets
    int_fast32_t b = INT_FAST32_MIN; // 4 ou +, le + rapide
    uint_least32_t c = UINT_LEAST32_MAX; // 4 ou +, le + petit
    cout << "./fast/least : " << sizeof(a) << "/"
        << sizeof(b) << "/" << sizeof(c) << endl;

    intmax_t d = INTMAX_MAX; // le + grand possible
    intptr_t e = INTPTR_MIN; // m�me taille que les pointeurs

    // a = 10; // decimal
    // a = 012; // octal
    // a = 0xA; // hexadecimal
    // a = 0b1010; // binaire
    // a = 1'000'000;
    a = 1'000'00; // attention!
}

void initializations() {
    cout << "inits : " << endl;
    int a = 8;
    int b(8);
    int c{8}; // accolade!
    cout << a << b << c << endl; // 888
    struct S1 { S1(int x){} };
    S1 d {9};
    S1 e {d};
    S1 f {c};

    int g (); // d�claration de fonction !
    int h {}; // init avec valeur par d�faut
    // int i = {}; // erreur

    int t1[3] { 8, -16, 43 };
    int t2[10] { 5, 4, 3, 2 };
    cout << "t2[6] = " << t2[6] << endl;

    vector<double> v1 {2.3, 4.9, 5.1 };
    cout << "v1[2] = " << v1[2] << endl;
    vector<double> v2 (3);
    cout << "v2[2] = " << v2[2] << endl; // 0, valeur par defaut
    vector<double> v3 {3};
    cout << "v3[0] = " << v3[0] << endl; // 3

    map<string, unsigned> collections { {"livre", 87822}, {"cd", 2401},
        {"dvd", 1190} };
    cout << collections["cd"] << " cd" << endl;

    struct S2 {
        int total { 0 };
        S2(initializer_list<int> l) {
            /*for(initializer_list<int>::iterator it = l.begin() ;
                it != l.end() ;
                it++) {
                    total += *it;   // *it est un entier
                }
            */
            for(const int x : l) {
                total += x;
            }
        }
    };
    S2 s { 3,2,5,4,3,5 };
    cout << s.total << endl; // 22

    cout << "t2 et v1 : " ;
    for(int x : t2)
        cout << x << ", ";
    for(double x : v1)
        cout << x << ", ";
}

void enums() {
    cout << "Enumerations : " << endl;
    enum Jours { Lundi, Mardi, Mercredi, Jeudi, Vendredi };
    Jours j1 = Mardi;
    enum Couleurs { Bleu, Vert, Rouge };
    Couleurs c1 = Vert;
    if(j1 == c1) {
        cout << "Pareil !?" << endl;
    }
    // interdit enum Albums { EnPassant, Minoritaire, UnTourEnsemble, Rouge };
    enum class Albums { EnPassant, Minoritaire, UnTourEnsemble, Rouge };
    enum class JoursC { Lundi, Mardi, Mercredi, Jeudi, Vendredi };
    enum class CouleursC { Bleu, Vert, Rouge };
    Albums a1 = Albums::Minoritaire;
    JoursC jc1 = JoursC::Mardi;
    // interdit : if(a1 == jc1) {
    //      cout << "Pareil !?" << endl;
    // }
    if(jc1 <= JoursC::Mercredi)
        cout << int(jc1) << " : Debut de semaine" << endl;
    if(int(jc1) <= 2)
        cout << "Debut de semaine" << endl;
}

void alignements() {
    cout << "Alignements" << endl;
    int32_t x, y; // . . . . y y y y x x x x . . .
    cout << " x : " << intptr_t(&x) << ", y : " << intptr_t(&y) << endl;
    cout << " x - alignement : " << alignof(x) << endl;

    struct alignas(16) S1 { int32_t a; int8_t b, c, d; int32_t e; };
    S1 s1; // .... a a a a b c d . e e e e
    cout << " a : " << intptr_t(&s1.a) << ", b : " << intptr_t(&s1.b) <<
        ", c : " << intptr_t(&s1.c) << " d : " << intptr_t(&s1.d) <<
        ", e : " << intptr_t(&s1.e) << endl;
    cout << " s1 - alignement : " << alignof(s1) << endl;
}

void autos() {
    cout << " Auto : " << endl;
    int32_t a = 72;
    auto b { a };
    cout << " b : " << sizeof(b) << endl; // 4 octets
    auto c = a;
    auto d ( a );
    cout << " c, d : " << sizeof(c) << "," << sizeof(d) << endl; // 4, 4
    // interdit : auto e { };
    // interdit : auto f { 2, 6, 22 };
    auto g = a + 2.3f + 6.2; // g : double
    cout << "g : " << typeid(g).name() << endl;

    vector<bool> v1 { true, true, false, true} ;
    for(auto x : v1) {
        cout << "* " << x << endl;
    }

    int32_t& ra = a;
    auto a2 = ra; // a2 n'est pas une r�f�rence sur ra
    a2 = 83;
    cout << "a=" << a << endl; // 72
    auto& ra2 = ra; // ra2 est une r�f�rence sur ra (et donc a)
    ra2 = 94;
    cout << "a=" << a << endl; // 94

    struct S1 {
        auto f1(int x) { if(x<0) return x*0.1;  else return x*4.0; }
    };
    S1 s1;
    auto h = s1.f1(8);
    cout << "h : " << typeid(h).name() << endl;

    decltype(a) i = 20;
    cout << "i : " << typeid(i).name() << endl; // i
    decltype(s1.f1(0)) j = 11 + s1.f1(4);
    cout << "j : " << typeid(j).name() << endl; // d

    decltype(auto) k = a; // si a est issu d'une template
}

void methodesAutomatiques() {
    struct S1 {
        string message;
        // S1() : message("") {}
        S1() = default;
        S1(string m) : message(m) {}
        S1& operator=(const S1& source) = delete;
    };
    S1 s1 { };
    s1.message = "Ok !";
    cout << s1.message << endl;
    S1 s2;
    // "use of deleted function" : s2 = s1;
    //s1.message = "Ok 2 !";
    // cout << s2.message << endl;
}

void ctorDelegues() {
    struct Usager {
        string prenom, nom;
        unsigned age;
        //Usager():prenom("inconnu"),nom("inconnu"),age(0) {}
        Usager(): Usager("inconnu") {}
        Usager(string p): Usager(p, "inconnu") {}
        Usager(string p, string n): Usager(p, n, 0) {}
        Usager(string p, string n, unsigned a): prenom(p),nom(n),age(a) {}
        void afficher() {
            cout << prenom << " " << nom << " " << age << " ans" << endl;
        }
    };

    Usager u1("Bob", "Martin", 8);
    u1.afficher();

    Usager u2;
    u2.nom = "Dupond";
    u2.prenom = "Ann";
    u2.age = 72;
    u2.afficher();

    Usager u3("Carl","Durand");
    Usager u4("Dana");
}

void methodesHeritees() {
    class Chariot {
    private:
        float vitesse;
    protected:
        Chariot():Chariot(1) {}
        void setVitesse() { vitesse = 0; }
        void setVitesse(float v) { vitesse = v; }
    public:
        Chariot(float v):vitesse(v) {}
    };

    Chariot c1(2.4f);
    // prot�g� :  Chariot c2;

    class ChariotElectrique : public Chariot {
    private:
        void setVitesse() { setVitesse(2); }
    public:
        using Chariot::Chariot;
        // neutralise tout le "using" ChariotElectrique(float v) {}
        using Chariot::setVitesse;
    };

    ChariotElectrique c2;
    ChariotElectrique c3(3.2f);
    c2.setVitesse(3.2f);
    // privee dans la fille : c3.setVitesse();
}

template<typename T> using PointeurVecteur = vector<T> *;
void alias() {
    unsigned int* p1;

    typedef unsigned int* PointeurNaturel;
    PointeurNaturel p2;

    using PointeurNaturel2 = unsigned int*;
    PointeurNaturel2 p3;

    vector<int> v1 { 0, 3, 2, 4};
    using VecteurEntiers = vector<int>;
    VecteurEntiers v2 = v1;

    PointeurVecteur<int> pv2 = &v2;
}

void assertions() {
    const int MAX_AGE = 120;
    static_assert(MAX_AGE > 0 && MAX_AGE < 1000, "MAX_AGE incorrect");
    cout << "Inscriptions - votre age ?";
    int age;
    cin >> age;
    // #include <cassert>
    assert(age > 0 && age < MAX_AGE);
}

void expressionsConstantes() {
    int x = 5;
    // interdit, pas C++ ISO : int t1[x];
    int t2[4];
    int t3[2+2];
    const int y = 6;
    int t4[y];
    int t5[y*2];

    struct S1 { int getV() { return 2; } };
    S1 s1;
    // interdit : int t6[s1.getV()];

    int z = y + 2;
    // interdit : int t7[z];

    constexpr int z2 = y + 2;
    int t8[z2];

    struct S2 { constexpr int getV() { return 2; } };
    S2 s2;
    int t9[s2.getV()];

    struct S3 { constexpr int getV() { int x = 1; return x; } };
    // interdit : struct S4 { constexpr int getV() { int x; x = 1; return x; } };

    struct S5 { constexpr int getV(int n) { return n*2; } };
    S5 s5;
    int t10[s5.getV(3)];
}

void nouveautes() {
    cout << "Nouveautes" << endl;
    // nullPtr();
    // types();
    // initializations();
    // enums();
    // alignements();
    // autos();
    // methodesAutomatiques();
    // ctorDelegues();
    // methodesHeritees();
    // alias();
    // assertions();
    expressionsConstantes();
}
